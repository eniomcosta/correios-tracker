package com.pos.unp.eniomcosta.correiostracker.crawler;

import android.os.AsyncTask;

import com.pos.unp.eniomcosta.correiostracker.domain.HistoricoRastreio;
import com.pos.unp.eniomcosta.correiostracker.domain.ItemHistorico;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CrawlerBuscaHistorico extends AsyncTask<String, Void, List<ItemHistorico>> {

    private final String URL_CORREIOS = "https://www2.correios.com.br/sistemas/rastreamento/ctrl/ctrlRastreamento.cfm";

    @Override
    protected List<ItemHistorico> doInBackground(String... codigoRastreio) {
        Connection.Response response = null;
        try {
            response = Jsoup.connect(URL_CORREIOS)
                    .method(Connection.Method.POST)
                    .timeout(100 * 1000)
                    .data("objetos", codigoRastreio[0])
                    .execute();

            Document doc = response.parse();

            List<ItemHistorico> historico = new ArrayList<ItemHistorico>();

            Element tableElement = doc.getElementsByClass("listEvent sro").get(0);

            for(Element tr : tableElement.getElementsByTag("tr")){
                ItemHistorico item = new ItemHistorico();

                item.setDataLocalEvento(tr.getElementsByClass("sroDtEvent").get(0).text());
                item.setDescricaoEvento(tr.getElementsByClass("sroLbEvent").get(0).text());

                historico.add(item);
            }

            return historico;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
