package com.pos.unp.eniomcosta.correiostracker.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.pos.unp.eniomcosta.correiostracker.domain.Encomenda;
import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

import java.util.List;

@Dao
public interface EncomendaDao {

    @Query("SELECT * FROM encomenda WHERE usuario_id = :usuarioId")
    List<Encomenda> findEncomendasByUsuario(int usuarioId);

    @Query("SELECT * FROM encomenda WHERE id = :id")
    Encomenda findEncomendaById(int id);

    @Query("SELECT COUNT(*) FROM encomenda WHERE usuario_id = :usuarioId")
    int countEncomendasByUsuario(int usuarioId);

    @Insert
    void insertAll(Encomenda... encomendas);

    @Delete
    void delete(Encomenda encomenda);

    @Update
    void update(Encomenda encomenda);
}
