package com.pos.unp.eniomcosta.correiostracker.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pos.unp.eniomcosta.correiostracker.R;
import com.pos.unp.eniomcosta.correiostracker.domain.ItemHistorico;

import java.util.List;

public class HistoricoRastreioAdapter extends RecyclerView.Adapter<HistoricoRastreioAdapter.HistoricoRastreioViewHolder> {
    private List<ItemHistorico> dataSet;
    private LayoutInflater layoutInflater;

    public static class HistoricoRastreioViewHolder extends RecyclerView.ViewHolder{
        public TextView dataLocalEvento;
        public TextView descricaoEvento;

        public HistoricoRastreioViewHolder(View view) {
            super(view);
            dataLocalEvento = (TextView) view.findViewById(R.id.txtDataLocalEvento);
            descricaoEvento = (TextView) view.findViewById(R.id.txtDescricaoEvento);
        }
    }

    public HistoricoRastreioAdapter(Context c, List<ItemHistorico> itemHistoricoList){
        this.dataSet = itemHistoricoList;
        this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public HistoricoRastreioViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_historico_rastreio, viewGroup, false);

        HistoricoRastreioViewHolder historicoRastreioViewHolder = new HistoricoRastreioViewHolder(v);
        return historicoRastreioViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricoRastreioViewHolder historicoRastreioViewHolder, final int i) {
        historicoRastreioViewHolder.dataLocalEvento.setText(dataSet.get(i).getDataLocalEvento());
        historicoRastreioViewHolder.descricaoEvento.setText(dataSet.get(i).getDescricaoEvento());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
