package com.pos.unp.eniomcosta.correiostracker;

import android.content.ContentValues;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.dbholder.AppDatabase;
import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

public class CadastroUsuarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);

        Button btnCadastrar = (Button) findViewById(R.id.btnCadastrar);
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome = ((TextView) findViewById(R.id.edtNome)).getText().toString();
                String login = ((TextView) findViewById(R.id.edtLogin)).getText().toString();
                String email = ((TextView) findViewById(R.id.edtEmail)).getText().toString();
                String senha = ((TextView) findViewById(R.id.edtSenha)).getText().toString();
                String confSenha = ((TextView) findViewById(R.id.edtConfSenha)).getText().toString();

                Usuario usuario = new Usuario(null,nome,login,email,senha,confSenha);

                if (validate(usuario)){

                    try {
                        addUsuario(AppDatabase.getAppDatabase(getApplicationContext()), usuario);

                        Toast.makeText(getApplicationContext(), "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();

                        finish();
                    }catch (SQLiteConstraintException e){
                        Toast.makeText(getApplicationContext(), "Erro: Usuário informado não disponível!", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private boolean validate(Usuario usuario){
        if (usuario.getLogin().isEmpty()){
            Toast.makeText(getApplicationContext(),"O campo Usuário é obrigatório", Toast.LENGTH_LONG).show();
            return false;
        }

        if (usuario.getEmail().isEmpty()){
            Toast.makeText(getApplicationContext(),"O campo E-mail é obrigatório", Toast.LENGTH_LONG).show();
            return false;
        }
        if (usuario.getSenha().isEmpty()){
            Toast.makeText(getApplicationContext(),"O campo Senha é obrigatório", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!usuario.getSenha().equals(usuario.getConfSenha())){
            Toast.makeText(getApplicationContext(),"As senhas não conferem!", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private static Usuario addUsuario(final AppDatabase db, Usuario usuario) {
        db.usuarioDao().insertAll(usuario);
        return usuario;
    }
}
