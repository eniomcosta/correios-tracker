package com.pos.unp.eniomcosta.correiostracker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.adapter.HistoricoRastreioAdapter;
import com.pos.unp.eniomcosta.correiostracker.crawler.CrawlerBuscaHistorico;
import com.pos.unp.eniomcosta.correiostracker.domain.ItemHistorico;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HistoricoRastreioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_rastreio);

        Bundle extras = getIntent().getExtras();

        String codigo = extras.getString("CODIGO_RASTREIO");
        String titulo = extras.getString("TITULO_ENCOMENDA");

        List<ItemHistorico> historico = searchHistoricoByCodigoRastreio(codigo);

        TextView txtTitulo = (TextView) findViewById(R.id.txtTituloHistorico);
        TextView txtCodigo = (TextView) findViewById(R.id.txtCodigoHistorico);

        txtTitulo.setText(titulo);
        txtCodigo.setText(codigo);

        final RecyclerView historicoRastreioRecycler = (RecyclerView) findViewById(R.id.historicoRastreioRecycler);
        historicoRastreioRecycler.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        historicoRastreioRecycler.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(historicoRastreioRecycler.getContext(),
                linearLayoutManager.getOrientation());
        historicoRastreioRecycler.addItemDecoration(dividerItemDecoration);

        HistoricoRastreioAdapter historicoRastreioAdapter = new HistoricoRastreioAdapter(getApplicationContext(),historico);
        historicoRastreioRecycler.setAdapter(historicoRastreioAdapter);

    }

    private List<ItemHistorico> searchHistoricoByCodigoRastreio(String codigoRastreio){
        CrawlerBuscaHistorico crawler = new CrawlerBuscaHistorico();
        try {
            List<ItemHistorico> result = crawler.execute(new String[] {codigoRastreio}).get();

            if (!result.isEmpty()) {
                return result;
            }else{

                Toast.makeText(getApplicationContext(),"Não foi encontrado nenhum histórico para o código de rastreio informado.", Toast.LENGTH_LONG).show();

                Intent it = new Intent(HistoricoRastreioActivity.this, ListaRastreioActivity.class);
                startActivity(it);

            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return new ArrayList<ItemHistorico>();

    }
}
