package com.pos.unp.eniomcosta.correiostracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AirplaneBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == Intent.ACTION_AIRPLANE_MODE_CHANGED) {
            Toast.makeText(context, "Modo avião alternado", Toast.LENGTH_SHORT).show();
        }
    }
}
