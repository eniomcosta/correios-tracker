package com.pos.unp.eniomcosta.correiostracker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.adapter.ListaRastreioAdapter;
import com.pos.unp.eniomcosta.correiostracker.dbholder.AppDatabase;
import com.pos.unp.eniomcosta.correiostracker.domain.Encomenda;
import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

import java.util.List;

public class ListaRastreioActivity extends AppCompatActivity {

    ListaRastreioAdapter listaRastreioAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_rastreio);

        validateUsuario();

        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Usuario usuarioLogado = findUsuarioById(AppDatabase.getAppDatabase(getApplicationContext()),spref.getInt("userId",0));

        List<Encomenda> encomendas = findEncomendasByUsuario(
                AppDatabase.getAppDatabase(getApplicationContext()),
                usuarioLogado);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(ListaRastreioActivity.this, CadastroRastreioActivity.class);
                startActivity(it);
            }
        });

        TextView txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        TextView txtCodigoRastreio = (TextView) findViewById(R.id.txtCodigoRastreio);

        final RecyclerView listaRastreioRecycler = (RecyclerView) findViewById(R.id.lista_rastreio_recycler);
        listaRastreioRecycler.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listaRastreioRecycler.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(listaRastreioRecycler.getContext(),
                linearLayoutManager.getOrientation());
        listaRastreioRecycler.addItemDecoration(dividerItemDecoration);

        listaRastreioAdapter = new ListaRastreioAdapter(getApplicationContext(),encomendas);
        listaRastreioRecycler.setAdapter(listaRastreioAdapter);

    }

    private void validateUsuario(){
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (spref.getInt("userId",0) == 0){
            Toast.makeText(getApplicationContext(),"Usuário não encontrado!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private List<Encomenda> findEncomendasByUsuario(AppDatabase db, Usuario usuario) {
        return db.encomendaDao().findEncomendasByUsuario(usuario.getId());
    }

    private Usuario findUsuarioById(AppDatabase db, Integer userId){
        return db.usuarioDao().findUsuarioById(userId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_options, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                listaRastreioAdapter.getFilter().filter(s);
                return false;
            }
        });
        return true;
    }
}
