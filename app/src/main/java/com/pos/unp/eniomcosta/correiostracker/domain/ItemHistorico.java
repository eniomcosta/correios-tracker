package com.pos.unp.eniomcosta.correiostracker.domain;

public class ItemHistorico {

    private String dataLocalEvento;
    private String descricaoEvento;

    public ItemHistorico(){}

    public ItemHistorico(String dataLocalEvento, String descricaoEvento) {
        this.dataLocalEvento = dataLocalEvento;
        this.descricaoEvento = descricaoEvento;
    }

    public String getDataLocalEvento() {
        return dataLocalEvento;
    }

    public void setDataLocalEvento(String dataLocalEvento) {
        this.dataLocalEvento = dataLocalEvento;
    }

    public String getDescricaoEvento() {
        return descricaoEvento;
    }

    public void setDescricaoEvento(String descricaoEvento) {
        this.descricaoEvento = descricaoEvento;
    }
}
