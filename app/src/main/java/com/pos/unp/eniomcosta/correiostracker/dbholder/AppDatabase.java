package com.pos.unp.eniomcosta.correiostracker.dbholder;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.pos.unp.eniomcosta.correiostracker.dao.EncomendaDao;
import com.pos.unp.eniomcosta.correiostracker.dao.UsuarioDao;
import com.pos.unp.eniomcosta.correiostracker.domain.Encomenda;
import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

@Database(entities = {Usuario.class, Encomenda.class}, version = 5, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract UsuarioDao usuarioDao();
    public abstract EncomendaDao encomendaDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "correios-tracker-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}