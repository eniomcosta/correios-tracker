package com.pos.unp.eniomcosta.correiostracker.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.HistoricoRastreioActivity;
import com.pos.unp.eniomcosta.correiostracker.R;
import com.pos.unp.eniomcosta.correiostracker.dbholder.AppDatabase;
import com.pos.unp.eniomcosta.correiostracker.domain.Encomenda;

import java.util.ArrayList;
import java.util.List;

public class ListaRastreioAdapter extends RecyclerView.Adapter<ListaRastreioAdapter.ListaRastreioViewHolder> implements Filterable {
    private List<Encomenda> dataSet;
    private List<Encomenda> dataSetFull;
    private LayoutInflater layoutInflater;

    public static class ListaRastreioViewHolder extends RecyclerView.ViewHolder{
        public TextView titulo;
        public TextView codigoRastreio;
        public ImageView imgDelete;
        public ImageView imgDetails;

        public ListaRastreioViewHolder(View view) {
            super(view);
            titulo = (TextView) view.findViewById(R.id.txtTitulo);
            codigoRastreio = (TextView) view.findViewById(R.id.txtCodigoRastreio);
            imgDelete = (ImageView) view.findViewById(R.id.imgDelete);
            imgDetails = (ImageView) view.findViewById(R.id.imgDetails);
        }
    }

    public ListaRastreioAdapter(Context c, List<Encomenda> encomendas){
        this.dataSet = encomendas;
        this.dataSetFull = new ArrayList<>(dataSet);
        this.layoutInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ListaRastreioViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = layoutInflater.inflate(R.layout.item_rastreio, viewGroup, false);

        ListaRastreioViewHolder listaRastreioViewHolder = new ListaRastreioViewHolder(v);
        return listaRastreioViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ListaRastreioViewHolder listaRastreioViewHolder, final int i) {
        listaRastreioViewHolder.titulo.setText(dataSet.get(i).getTitulo());
        listaRastreioViewHolder.codigoRastreio.setText(dataSet.get(i).getCodigoRastreio());

        final ListaRastreioViewHolder holder = listaRastreioViewHolder;

        listaRastreioViewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Encomenda encomendaToDelete = dataSet.get(i);

                AppDatabase.getAppDatabase(v.getContext()).encomendaDao().delete(encomendaToDelete);

                dataSet.remove(i);
                notifyItemRemoved(i);
            }
        });

        listaRastreioViewHolder.imgDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isOnline(v.getContext())) {

                    String codigo = dataSet.get(i).getCodigoRastreio();
                    String titulo = dataSet.get(i).getTitulo();

                    Intent it = new Intent(v.getContext(), HistoricoRastreioActivity.class);
                    it.putExtra("CODIGO_RASTREIO", codigo);
                    it.putExtra("TITULO_ENCOMENDA", titulo);

                    holder.itemView.getContext().startActivity(it);

                }else{
                    Toast.makeText(v.getContext(), "Sem conexão com a internet", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public Filter getFilter() {
        return listaRastreioFilter;
    }

    private Filter listaRastreioFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Encomenda> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0){
                filteredList.addAll(dataSetFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Encomenda encomenda : dataSetFull){
                    if (encomenda.getTitulo().toLowerCase().contains(filterPattern)){
                        filteredList.add(encomenda);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataSet.clear();
            dataSet.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return (netInfo != null && netInfo.isConnected());
    }
}
