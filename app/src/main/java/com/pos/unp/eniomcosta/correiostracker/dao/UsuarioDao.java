package com.pos.unp.eniomcosta.correiostracker.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

import java.util.List;

@Dao
public interface UsuarioDao {

    @Query("SELECT * FROM usuario")
    List<Usuario> findAll();

    @Query("SELECT COUNT(*) FROM usuario")
    int countUsuarios();

    @Query("SELECT * FROM usuario WHERE login = :login AND senha = :senha")
    Usuario checkUsuario(String login, String senha);

    @Insert
    void insertAll(Usuario... usuarios);

    @Delete
    void delete(Usuario usuario);

    @Query("SELECT * FROM usuario WHERE id = :userId")
    Usuario findUsuarioById(Integer userId);
}
