package com.pos.unp.eniomcosta.correiostracker.crawler;

import android.os.AsyncTask;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class CrawlerValidateCodigo extends AsyncTask<String, Void, Boolean> {

    private final String URL_CORREIOS = "https://www2.correios.com.br/sistemas/rastreamento/ctrl/ctrlRastreamento.cfm";

    @Override
    protected Boolean doInBackground(String... codigoRastreio) {
        Connection.Response response = null;
        try {
            response = Jsoup.connect(URL_CORREIOS)
                    .method(Connection.Method.POST)
                    .timeout(100 * 1000)
                    .data("objetos", codigoRastreio[0])
                    .execute();

            Document doc = response.parse();

            if (doc.getElementsByClass("codSro").size() > 0){
                return true;
            } else{
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
