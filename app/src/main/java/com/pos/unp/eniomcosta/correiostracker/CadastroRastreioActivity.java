package com.pos.unp.eniomcosta.correiostracker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.crawler.CrawlerValidateCodigo;
import com.pos.unp.eniomcosta.correiostracker.dbholder.AppDatabase;
import com.pos.unp.eniomcosta.correiostracker.domain.Encomenda;
import com.pos.unp.eniomcosta.correiostracker.domain.ItemHistorico;

import org.w3c.dom.Text;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class CadastroRastreioActivity extends AppCompatActivity {

    private Encomenda encomenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_rastreio);

        validateUsuario();

        final Button btnCadastrar = (Button) findViewById(R.id.btnCadastrarRastreio);
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isOnline(getApplicationContext())){

                // hide virtual keyboard
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(btnCadastrar.getWindowToken(),
                        InputMethodManager.RESULT_UNCHANGED_SHOWN);

                String titulo = ((EditText) findViewById(R.id.edtTitulo)).getText().toString();
                String codigoRastreio = ((EditText) findViewById(R.id.edtCodigoRastreio)).getText().toString();

                SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                Integer userId = spref.getInt("userId",0);

                Encomenda encomenda = new Encomenda(titulo,codigoRastreio,userId);

                if (!validateForm(encomenda)){
                    Snackbar.make(v, "Preencha os campos 'Titulo' e 'Código rastreio'.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else {

                    if (validateCodigoRastreio(codigoRastreio)) {
                         addEncomenda(AppDatabase.getAppDatabase(getApplicationContext()), encomenda, v);

                        Intent it = new Intent(CadastroRastreioActivity.this, ListaRastreioActivity.class);
                        startActivity(it);
                    } else {
                        Snackbar.make(v, "Código informado inválido/inexistente.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }

                } else {
                    Toast.makeText(getApplicationContext(),"Sem conexão com a internet", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void validateUsuario(){
        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (spref.getInt("userId",0) == 0){
            Toast.makeText(getApplicationContext(),"Usuário não encontrado!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private boolean validateCodigoRastreio(String codigoRastreio){
        CrawlerValidateCodigo crawler = new CrawlerValidateCodigo();
        try {
            boolean result = crawler.execute(new String[] {codigoRastreio}).get();

            if (result) {
                return true;
            }else{
                return false;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;

    }

    public boolean validateForm(Encomenda encomenda){
        if (encomenda.getTitulo().equals("")){
            return false;
        }
        if (encomenda.getCodigoRastreio().equals("")){
            return false;
        }

        return true;
    }

    private static Encomenda addEncomenda(final AppDatabase db, Encomenda encomenda, View v) {
        try {
            db.encomendaDao().insertAll(encomenda);
        } catch (SQLiteConstraintException e) {
            Snackbar.make(v, "O código informado já se encontra cadastrado.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
        return encomenda;
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return (netInfo != null && netInfo.isConnected());
    }

}
