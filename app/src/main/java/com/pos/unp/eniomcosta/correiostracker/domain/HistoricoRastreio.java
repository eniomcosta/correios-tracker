package com.pos.unp.eniomcosta.correiostracker.domain;

import java.util.List;

public class HistoricoRastreio {

    private String status;
    private List<ItemHistorico> listaHistorico;

    public HistoricoRastreio(){}

    public HistoricoRastreio(String status, List<ItemHistorico> listaHistorico) {
        this.status = status;
        this.listaHistorico = listaHistorico;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ItemHistorico> getListaHistorico() {
        return listaHistorico;
    }

    public void setListaHistorico(List<ItemHistorico> listaHistorico) {
        this.listaHistorico = listaHistorico;
    }
}
