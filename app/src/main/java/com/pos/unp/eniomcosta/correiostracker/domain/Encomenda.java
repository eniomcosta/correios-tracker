package com.pos.unp.eniomcosta.correiostracker.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;
import java.util.Objects;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "encomenda", indices = {@Index(value = {"codigo_rastreio","usuario_id"}, unique = true)},
foreignKeys = @ForeignKey(entity = Usuario.class, parentColumns = "id", childColumns = "usuario_id", onDelete = CASCADE))
public class Encomenda implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @ColumnInfo(name = "titulo")
    private String titulo;

    @ColumnInfo(name = "codigo_rastreio")
    private String codigoRastreio;

    @ColumnInfo(name = "usuario_id")
    private Integer usuarioId;

    public Encomenda(){}

    public Encomenda(String titulo, String codigoRastreio, Integer usuarioId) {
        this.titulo = titulo;
        this.codigoRastreio = codigoRastreio;
        this.usuarioId = usuarioId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getCodigoRastreio() {
        return codigoRastreio;
    }

    public void setCodigoRastreio(String codigoRastreio) {
        this.codigoRastreio = codigoRastreio;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Encomenda encomenda = (Encomenda) o;
        return Objects.equals(id, encomenda.id) &&
                Objects.equals(titulo, encomenda.titulo) &&
                Objects.equals(codigoRastreio, encomenda.codigoRastreio) &&
                Objects.equals(usuarioId, encomenda.usuarioId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, titulo, codigoRastreio, usuarioId);
    }
}
