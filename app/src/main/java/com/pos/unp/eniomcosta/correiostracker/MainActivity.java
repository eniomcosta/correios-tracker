package com.pos.unp.eniomcosta.correiostracker;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pos.unp.eniomcosta.correiostracker.dbholder.AppDatabase;
import com.pos.unp.eniomcosta.correiostracker.domain.Usuario;

public class MainActivity extends AppCompatActivity {

    AirplaneBroadcast airplaneBroadcastbroadCastReceiver;
    NetworkChangeReceiver networkChangeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        airplaneBroadcastbroadCastReceiver=new AirplaneBroadcast();
        this.registerReceiver(airplaneBroadcastbroadCastReceiver,new IntentFilter("android.intent.action.AIRPLANE_MODE"));

        networkChangeReceiver=new NetworkChangeReceiver();
        this.registerReceiver(networkChangeReceiver,new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        Button btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener( new View.OnClickListener(){
              @Override
              public void onClick(View v) {
                  String login = ((TextView) findViewById(R.id.edtLogin)).getText().toString();
                  String senha = ((TextView) findViewById(R.id.edtSenha)).getText().toString();

                  Usuario usuarioLogado = new Usuario(login,senha);

                  usuarioLogado = findUsuario(AppDatabase.getAppDatabase(getApplicationContext()),usuarioLogado);

                  if ( null != usuarioLogado) {
                      Intent it = new Intent(MainActivity.this, ListaRastreioActivity.class);
                      it.putExtra("Usuario", usuarioLogado);
                      it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                      SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                      SharedPreferences.Editor sprefEditor = spref.edit();
                      sprefEditor.putInt("userId",usuarioLogado.getId());
                      sprefEditor.apply();

                      startActivity(it);
                  } else {
                      Toast.makeText(getApplicationContext(),"Usuário não encontrado!",Toast.LENGTH_LONG).show();
                  }
              }
          }
        );


    }

    public void onClick(View v){
        Intent it = new Intent(MainActivity.this, CadastroUsuarioActivity.class);
        startActivity(it);
    }

    private static Usuario findUsuario(final AppDatabase db, Usuario usuario) {
        return db.usuarioDao().checkUsuario(usuario.getLogin(),usuario.getSenha());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.unregisterReceiver(airplaneBroadcastbroadCastReceiver);
        this.unregisterReceiver(networkChangeReceiver);
    }
}
